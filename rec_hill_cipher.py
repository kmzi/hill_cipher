from hill_cipher import m, rus_alph, not_coprime_numbers, egcd, mod_inverse, get_inv_key #adjoint_matrix
import numpy as np


def prepare_text(text):
    text = text.lower()#.replace(' ', '')
    dim = 3
    # добиваем блоки
    text = list(text)
    while len(text) % dim != 0:
        text.append('_')
    # переводим буквы в цифры
    for i in range(len(text)):
        text[i] = rus_alph.index(text[i])

    # разбиваем текст на блоки:
    chunked_text = []
    block = []
    for i in range(text.__len__()):
        block.append(text[i])
        if (i + 1) % dim == 0:
            chunked_text.append(block[:])  # copy! not a pointer => therefore [:]
            block.clear()
    return chunked_text


def encrypt_text(key1, key2, text):
    try:
        det1 = int(round(np.linalg.det(key1)) % m)
        det2 = int(round(np.linalg.det(key2)) % m)
        if (det1 * det2 == 0) or \
                det1 in not_coprime_numbers or det2 in not_coprime_numbers:  # проверка на взаим. прост. и определитель != 0
            raise np.linalg.LinAlgError
        new_text = []
        text = prepare_text(text)
        keys = [key1, key2]

        # шифровка
        for block, key in zip(text[:2], keys[:2]):
            encr_block = np.dot(key, np.asarray(block)) % m
            for char in encr_block:
                new_text.append(rus_alph[char])
        for block in text[2:]:
            keys.append(np.dot(keys[-1], keys[-2]) % m)
            encr_block = np.dot(keys[-1], np.asarray(block)) % m
            for char in encr_block:
                new_text.append(rus_alph[char])
        return ''.join(new_text)

    except np.linalg.LinAlgError:
        print('Ошибка! Неподходящий ключ.')
    except ValueError:  # ValueError: object is not in list (если зн. преп. из текста нет в алф)
        print('В тексте присутствуют недопустимые знаки, попробуйте снова.')


def decrypt_text(key1, key2, text):
    try:
        det1 = int(round(np.linalg.det(key1)) % m)
        det2 = int(round(np.linalg.det(key2)) % m)
        if (det1 * det2 == 0) or det1 in not_coprime_numbers or det2 in not_coprime_numbers:  # проверка на взаим. прост. и определитель != 0
            raise np.linalg.LinAlgError
        new_text = []
        text = prepare_text(text)
        inv_keys = [get_inv_key(key1, det1), get_inv_key(key2, det2)]
        # расшифровка
        for block, key in zip(text[:2], inv_keys[:2]):
            decr_block = np.dot(key, np.asarray(block)) % m
            for char in decr_block:
                new_text.append(rus_alph[char])
        for block in text[2:]:
            inv_keys.append(np.dot(inv_keys[-2], (inv_keys[-1])) % m)
            decr_block = np.dot(inv_keys[-1], np.asarray(block)) % m
            for char in decr_block:
                new_text.append(rus_alph[char])

        return ''.join(new_text)
    except np.linalg.LinAlgError:
        print('Ошибка! Неподходящий ключ.')
    except ValueError:  # ValueError: object is not in list (если зн. преп. из текста нет в алф)
        print('В тексте присутствуют недопустимые знаки, попробуйте снова.')


# text = 'капибара'  # 'деёлтохгм'
# key1 = np.array([[5, 4, 13], [8, 21, 11], [1, 6, 2]])
# key2 = np.array([[6, 8, 10], [9, 0, 1], [9, 7, 11]])
# encr = encrypt_text(key1, key2, text)
# print(encr)
# decr = decrypt_text(key1, key2, encr)

def main():
    while True:
        try:
            answer = (input('Choose your option: encrypt(1) or decrypt(2) for russian language: '))
            if answer not in ['1', '2']:
                print('Try again')
                continue

            prekey1 = list(map(int, input('Enter the first key (3x3 matrix: ints separated by space): ').split(' ')))
            key1 = np.asarray([prekey1[0:3], prekey1[3:6], prekey1[6:9]])
            prekey2 = list(map(int, input('Enter the second key (3x3 matrix: ints separated by space): ').split(' ')))
            key2 = np.asarray([prekey2[0:3], prekey2[3:6], prekey2[6:9]])

            text = input('Enter the text: ')
            if answer == '1':
                print('Encrypted text: ', encrypt_text(key1, key2, text))
            elif answer == '2':
                print('Decrypted text: ', decrypt_text(key1, key2, text))

        except ValueError:
            print('Wrong key, try again')  # qw or sth went wrong


if __name__ == '__main__':
    main()

import numpy as np

# https://www.geeksforgeeks.org/hill-cipher/
# http://cs231n.github.io/python-numpy-tutorial/#numpy-math


# todo: проверка обратимости матрицы
# todo: проверка определителя != 0
# todo: проверка взаимно простых чисел (определитель и 33?)
# todo: везде по модулю

# todo: деление по блокам - добивать блоки

# todo: exceptions
# todo: wrong input - how??


# m = 33
# coprime_numbers = list(filter(lambda i: i % 3 != 0 and i % 11 != 0, range(33)))
# rus_alph = list('абвгдеёжзийклмнопрстуфхцчшщъыьэюя')

# со знаками препинания

# rus_alph = list('абвгдеёжзийклмнопрстуфхцчшщъыьэюя,._')
# m = rus_alph.__len__()  # 36
# coprime_numbers = list(
#     filter(lambda i: i % 2 != 0 and i % 3 != 0 and i % 4 != 0 and i % 6 != 0 and i % 9 != 0 and i % 12 != 0
#            , range(33)))

rus_alph = list('абвгдеёжзийклмнопрстуфхцчшщъыьэюя,. _')
m = rus_alph.__len__()  # 37
not_coprime_numbers = [37]

for i, char in zip(range(37), rus_alph):
    print(f'{char}: {i}', end='\t')
    if i % 3 == 0:
        print()


# расширен. алг. Евклида
def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)


def mod_inverse(a, m=m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return x % m


def adjoint_matrix(matrix):
    adj_matrix = []
    adj_matrix.append([matrix[1][1] * matrix[2][2] - matrix[1][2] * matrix[2][1],
                       -(matrix[1][0] * matrix[2][2] - matrix[1][2] * matrix[2][0]),
                       matrix[1][0] * matrix[2][1] - matrix[1][1] * matrix[2][0]])

    adj_matrix.append([-(matrix[0][1] * matrix[2][2] - matrix[0][2] * matrix[2][1]),
                       matrix[0][0] * matrix[2][2] - matrix[0][2] * matrix[2][0],
                       -(matrix[0][0] * matrix[2][1] - matrix[2][0] * matrix[0][1])])

    adj_matrix.append([matrix[0][1] * matrix[1][2] - matrix[0][2] * matrix[1][1],
                       -(matrix[0][0] * matrix[1][2] - matrix[0][2] * matrix[1][0]),
                       matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0]])

    return np.asarray(adj_matrix).T % m


def get_inv_key(key, det):
    return adjoint_matrix(key) * mod_inverse(det) % m


def encrypt_message(key, plain_text):
    try:
        plain_text = plain_text.lower()
        det = int(round(np.linalg.det(key)) % m)
        if det == 0 or det in not_coprime_numbers:  # проверка на взаим. прост. и определитель != 0
            raise np.linalg.LinAlgError
        # remove spaces
        #plain_text = plain_text.replace(' ', '')
        plain_text = list(plain_text)
        dim = int(key.size ** 0.5)  # размерность м-цы
        # добиваем текст до кратности размерности ключа
        # для последующего деления на блоки
        while len(plain_text) % dim != 0:
            plain_text.append('_')
        # переводим буквы в цифры
        for i in range(len(plain_text)):
            plain_text[i] = rus_alph.index(plain_text[i])

        # разбиваем текст на блоки:
        chunked_text = []
        block = []
        for i in range(plain_text.__len__()):
            block.append(plain_text[i])
            if (i + 1) % dim == 0:
                chunked_text.append(block[:])  # copy! not a pointer => therefore [:]
                block.clear()
        # шифровка
        '''
        Y = KX
        '''
        encrypted_text = []
        for block in chunked_text:
            encr_block = np.dot(key, np.asarray(block)) % m
            for char in encr_block:
                encrypted_text.append(rus_alph[char])

        return ''.join(encrypted_text)

    except np.linalg.LinAlgError:
        print('Ошибка! Неподходящий ключ.')
    except ValueError:  # ValueError: object is not in list (если зн. преп. из текста нет в алф)
        print('В тексте присутствуют недопустимые знаки, попробуйте снова.')


def decrypt_message(key, encr_text):
    '''
    :param K: key
    :param Y: encrypted text
    :return: decrypted text
    '''
    try:
        det = int(round(np.linalg.det(key)) % m)
        if det == 0 or det in not_coprime_numbers:  # проверка на взаим. прост. и определитель != 0
            raise np.linalg.LinAlgError

        encr_text = encr_text.lower()
        # remove spaces
        # encr_text = encr_text.replace(' ', '')

        encr_text = list(encr_text)
        dim = int(key.size ** 0.5)  # размерность м-цы
        # # добиваем блоки до кратности размерности ключа
        # while len(encr_text) % dim != 0:
        #     encr_text.append('_')
        # todo: exception из-за нехватки символов в блоках зашифрованного текста?
        # переводим буквы в цифры
        for i in range(len(encr_text)):
            encr_text[i] = rus_alph.index(encr_text[i])

        # разбиваем текст на блоки:
        chunked_text = []
        block = []
        for i in range(encr_text.__len__()):
            block.append(encr_text[i])
            if (i + 1) % dim == 0:
                chunked_text.append(block[:])  # copy! not a pointer => therefore [:]
                block.clear()

        # расшифровка
        '''
        X = K^(-1)Y
        '''
        # qw
        '''
        K^(-1) = (det(K))(-1) * K^(*T)
        '''
        # print('mod_inv: ', mod_inverse(det))
        # print('adj matrix: ', adjoint_matrix(key))
        inv_key = get_inv_key(key, det)

        dectypted_text = []
        for block in chunked_text:
            decr_block = np.dot(inv_key, np.asarray(block)) % m

            for char in decr_block:
                dectypted_text.append(rus_alph[int(char)])

        return ''.join(dectypted_text)

    except np.linalg.LinAlgError:
        print('Ошибка! Неподходящий ключ.')
    except ValueError:  # ValueError: object is not in list (если зн. преп. из текста нет в алф)
        print('В тексте присутствуют недопустимые знаки, попробуйте снова.')


def main():
    while True:
        try:
            answer = (input('Choose your option: encrypt(1) or decrypt(2) for russian language: '))
            if answer not in ['1', '2']:
                print('Try again')
                continue

            prekey = list(map(int, input('Enter the key (3x3 matrix: ints separated by space): ').split(' ')))
            key = np.asarray([prekey[0:3], prekey[3:6], prekey[6:9]])
            # print(key)
            # if a1 not in coprime_numbers or a2 not in coprime_numbers:
            #     raise Exception

            text = input('Enter the text: ')
            if answer == '1':
                print('Encrypted text: ', encrypt_message(key, text))
            elif answer == '2':
                print('Decrypted text: ', decrypt_message(key, text))

        except ValueError:
            print('Wrong key, try again')  # qw or sth went wrong
        # except Exception:
        #     print(f'Your a in the key must be a coprime number with m = {m}')


if __name__ == '__main__':
    main()


#A = np.array([[2, 3, 4], [8, 12, 6], [5, 10, 1]], dtype=int)  # , dtype=np.int)

#key = A
#text = 'Привет, мир, как дела?'

# text = 'капибара'  # 'деёлтохгм'
# key = np.array([[5, 4, 13], [8, 21, 11], [1, 6, 2]])
#encr = encrypt_message(key, text)
#decr = decrypt_message(key, encr)
